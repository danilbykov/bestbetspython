from api import Api

if __name__ == "__main__":
    api = Api()
    print(api.load_bookmakers())
    print(api.load_countries())
    print(api.load_teams(5))
    print(api.load_tournaments())

    for snapshot in api.load_tournament_snapshot(81):
        print("\n")
        print(snapshot)

    #print(api.load_open_match_history(81, 100))

    #g = api.make_history_generator(81, 1)
    #print("\n".join(map(lambda x: str(x), g)))

    #print(api.load_match_history(81, 87))
