import requests

from data import MatchHistory, MatchSnapshot
from model import Bookmaker, Country, Match, Team, Tournament

class Api:
    def __init__(self):
        self.root = "https://ec2-52-56-215-130.eu-west-2.compute.amazonaws.com:8443/rest/"
        self.verify = False

    def load_bookmakers(self):
        json = requests.get(self.root + "bookmakers", verify = self.verify).json()
        return list(map(lambda x: Bookmaker(x), json))

    def load_countries(self):
        json = requests.get(self.root + "countries", verify = self.verify).json()
        return list(map(lambda x: Country(x), json))

    def load_teams(self, country_id):
        url = "{}countries/{}/teams".format(self.root, country_id)
        json = requests.get(url, verify = self.verify).json()
        return list(map(lambda x: Team(x), json))

    def load_tournaments(self):
        json = requests.get(self.root + "tournaments", verify = self.verify).json()
        return list(map(lambda x: Tournament(x), json))

    def load_tournament_snapshot(self, tournament_id):
        url = "{}tournaments/{}/matches/open".format(self.root, tournament_id)
        json = requests.get(url, verify = self.verify).json()
        return list(map(lambda x: MatchSnapshot(x), json))

    def load_open_match_history(self, tournament_id, match_id):
        url = "{}tournaments/{}/matches/open/{}".format(self.root, tournament_id, match_id)
        return MatchHistory(requests.get(url, verify = self.verify).json())

    def make_history_generator(self, tournament_id, count, from_time = pow(2, 62)):
        urlPattern = "{}tournaments/{}/matches/history?from={:.0f}&count={:.0f}"
        while True:
            url = urlPattern.format(self.root, tournament_id, from_time, count)
            json = requests.get(url, verify = self.verify).json()
            result = list(map(lambda x: Match(x), json))
            if len(result) == 0:
                break
            else:
                from_time = result[-1].get_start()
                yield result

    def load_match_history(self, tournament_id, match_id):
        url = "{}tournaments/{}/matches/history/{}".format(self.root, tournament_id, match_id)
        return MatchHistory(requests.get(url, verify = self.verify).json())
