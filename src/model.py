from datetime import datetime

class Bookmaker:
    def __init__(self, json):
        self._id = json['id']
        self._name = json['name']

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def __repr__(self):
        return "<Bookmaker id={} name={}/>".format(self._id, self._name)


class Country:
    def __init__(self, json):
        self._id = json['id']
        self._name = json['name']

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def __repr__(self):
        return "<Country id={} name={}/>".format(self._id, self._name)


class Team:
    def __init__(self, json):
        self._id = json['id']
        self._name = json['name']

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def __repr__(self):
        return "<Team id={} name={}/>".format(self._id, self._name)


class Tournament:
    def __init__(self, json):
        self._id = json['id']
        self._name = json['name']
        self._country = json['country']

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def get_country(self):
        return self._country

    def __repr__(self):
        pattern = "<Tournament id={} name={} country={}/>"
        return pattern.format(self._id, self._name, self._country)


class Match:
    def __init__(self, json):
        self._id = json['id']
        self._home_team_id = json['homeTeamId']
        self._away_team_id = json['awayTeamId']
        self._start = json['start']
        self._tournament_id = json['tournamentId']

    def get_id(self):
        return self._id

    def get_home_team_id(self):
        return self._home_team_id

    def get_away_team_id(self):
        return self._away_team_id

    def get_start(self):
        return self._start

    def get_tournament_id(self):
        return self._tournament_id

    def __repr__(self):
        pattern = "<Match id={} name={} vs {} start={} tournament={} />"
        return pattern.format(self._id,
            self._home_team_id, self._away_team_id,
            datetime.fromtimestamp(self._start / 1000).strftime("%m-%d %H:%M"),
            self._tournament_id
        )
