from datetime import datetime
from model import Match

class TwoLineBets:
    def __init__(self, json):
        self._home_win = json['homeWin']
        self._away_win = json['awayWin']

    def get_home_win(self):
        return self._home_win

    def get_away_win(self):
        return self._away_win

    def __repr__(self):
        return "(1={:.3f} 2={:.3f})".format(self._home_win, self._away_win)

class ThreeLineBets:
    def __init__(self, json):
        self._home_win = json['homeWin']
        self._draw = json['draw']
        self._away_win = json['awayWin']

    def get_home_win(self):
        return self._home_win

    def get_draw(self):
        return self._draw

    def get_away_win(self):
        return self._away_win

    def __repr__(self):
        return "(1={:.3f} x={:.3f} 2={:.3f})".format(self._home_win, self._draw, self._away_win)

class BookmakerSnapshot:
    def __init__(self, json):
        self._home_win = json['homeWin']
        self._draw = json['draw']
        self._away_win = json['awayWin']
        self._not_home_win = json['notHomeWin']
        self._not_draw = json['notDraw']
        self._not_away_win = json['notAwayWin']
        self._draw_no_bet_home = json['drawNoBetHome']
        self._draw_no_bet_away = json['drawNoBetAway']
        self._both_teams_score = json['bothTeamsScore']
        self._both_teams_not_score = json['bothTeamsNotScore']
        self._odd_goals = json['oddGoals']
        self._even_goals = json['evenGoals']
        self._correct_scores = json['correctScores']
        self._over_totals = json['overTotals']
        self._under_totals = json['underTotals']
        self._asian_handicaps = {}
        self._european_handicaps = {}
        for key, val in json['asianHandicaps'].items():
            self._asian_handicaps[key] = TwoLineBets(val)
        for key, val in json['europeanHandicaps'].items():
            self._european_handicaps[key] = ThreeLineBets(val)

    def get_home_win(self):
        return self._home_win

    def get_draw(self):
        return self._draw

    def get_away_win(self):
        return self._away_win

    def get_not_home_win(self):
        return self._not_home_win

    def get_not_draw(self):
        return self._not_draw

    def get_not_away_win(self):
        return self._not_away_win

    def get_draw_no_bet_home(self):
        return self._draw_no_bet_home

    def get_draw_no_bet_away(self):
        return self._draw_no_bet_away

    def get_both_teams_score(self):
        return self._both_teams_score

    def get_both_teams_not_score(self):
        return self._both_teams_not_score

    def get_odd_goals(self):
        return self._odd_goals

    def get_even_goals(self):
        return self._even_goals

    def get_correct_scores(self):
        return self._correct_scores

    def get_over_totals(self):
        return self._over_totals

    def get_under_totals(self):
        return self._under_totals

    def get_asian_handicaps(self):
        return self._asian_handicaps

    def get_european_handicaps(self):
        return self._european_handicaps

    def __repr__(self):
        pattern = "<BookmakerSnapshot 1={:.3f} x={:.3f} 2={:.3f} 1x={:.3f} 12={:.3f} x2={:.3f} " \
            "w1={:.3f} w2={:.3f} score={:.3f} notscore={:.3f} odd={:.3f} even={:.3f}\n" \
            "correctscores={}\novertotals={}\nundertotals={}\nasianhandicaps={}\neuropeanhandicaps={} />"
        return pattern.format(self._home_win, self._draw, self._away_win,
            self._not_away_win, self._not_draw, self._not_home_win,
            self._draw_no_bet_home, self._draw_no_bet_away,
            self._both_teams_score, self._both_teams_not_score,
            self._odd_goals, self._even_goals,
            self._correct_scores, self._over_totals, self._under_totals,
            self._asian_handicaps, self._european_handicaps
        )




class HistoricalBet:
    def __init__(self, json):
        self._time = json['time']
        self._value = json['value']

    def get_time(self):
        return self._time

    def get_value(self):
        return self._value

    def __repr__(self):
        return "({:.3f} {})".format(self._value, datetime.fromtimestamp(self._time / 1000).strftime("%m-%d %H:%M"))

class TwoLineHistory:
    def __init__(self, json):
        print(json)
        self._home_win = list(map(lambda x: HistoricalBet(x), json['homeWin']))
        self._away_win = list(map(lambda x: HistoricalBet(x), json['awayWin']))

    def get_home_win(self):
        return self._home_win

    def get_away_win(self):
        return self._away_win

    def __repr__(self):
        return "(1={} 2={})".format(self._home_win, self._away_win)

class ThreeLineHistory:
    def __init__(self, json):
        print(json)
        self._home_win = list(map(lambda x: HistoricalBet(x), json['homeWin']))
        self._draw = list(map(lambda x: HistoricalBet(x), json['draw']))
        self._away_win = list(map(lambda x: HistoricalBet(x), json['awayWin']))

    def get_home_win(self):
        return self._home_win

    def get_draw(self):
        return self._draw

    def get_away_win(self):
        return self._away_win

    def __repr__(self):
        return "(1={} x={} 2={})".format(self._home_win, self._draw, self._away_win)


class BookmakerHistory:
    def __init__(self, json):
        self._home_win = list(map(lambda x: HistoricalBet(x), json['homeWin']))
        self._draw = list(map(lambda x: HistoricalBet(x), json['draw']))
        self._away_win = list(map(lambda x: HistoricalBet(x), json['awayWin']))
        self._not_home_win = list(map(lambda x: HistoricalBet(x), json['notHomeWin']))
        self._not_draw = list(map(lambda x: HistoricalBet(x), json['notDraw']))
        self._not_away_win = list(map(lambda x: HistoricalBet(x), json['notAwayWin']))
        self._draw_no_bet_home = list(map(lambda x: HistoricalBet(x), json['drawNoBetHome']))
        self._draw_no_bet_away = list(map(lambda x: HistoricalBet(x), json['drawNoBetAway']))
        self._both_teams_score = list(map(lambda x: HistoricalBet(x), json['bothTeamsScore']))
        self._both_teams_not_score = list(map(lambda x: HistoricalBet(x), json['bothTeamsNotScore']))
        self._odd_goals = list(map(lambda x: HistoricalBet(x), json['oddGoals']))
        self._even_goals = list(map(lambda x: HistoricalBet(x), json['evenGoals']))
        self._correct_scores = {}
        self._over_totals = {}
        self._under_totals = {}
        for key, val in json['correctScores'].items():
            self._correct_scores[key] = list(map(lambda x: HistoricalBet(x), val))
        for key, val in json['overTotals'].items():
            self._over_totals[key] = list(map(lambda x: HistoricalBet(x), val))
        for key, val in json['underTotals'].items():
            self._under_totals[key] = list(map(lambda x: HistoricalBet(x), val))
        self._asian_handicaps = {}
        self._european_handicaps = {}
        for key, val in json['asianHandicaps'].items():
            self._asian_handicaps[key] = TwoLineHistory(val)
        for key, val in json['europeanHandicaps'].items():
            self._european_handicaps[key] = ThreeLineHistory(val)

    def get_home_win(self):
        return self._home_win

    def get_draw(self):
        return self._draw

    def get_away_win(self):
        return self._away_win

    def get_not_home_win(self):
        return self._not_home_win

    def get_not_draw(self):
        return self._not_draw

    def get_not_away_win(self):
        return self._not_away_win

    def get_draw_no_bet_home(self):
        return self._draw_no_bet_home

    def get_draw_no_bet_away(self):
        return self._draw_no_bet_away

    def get_both_teams_score(self):
        return self._both_teams_score

    def get_both_teams_not_score(self):
        return self._both_teams_not_score

    def get_odd_goals(self):
        return self._odd_goals

    def get_even_goals(self):
        return self._even_goals

    def get_correct_scores(self):
        return self._correct_scores

    def get_over_totals(self):
        return self._over_totals

    def get_under_totals(self):
        return self._under_totals

    def get_asian_handicaps(self):
        return self._asian_handicaps

    def get_european_handicaps(self):
        return self._european_handicaps

    def __repr__(self):
        def dump_map(m):
            result = []
            for key, val in m.items():
                if len(val) != 0:
                    result.append("\t\t\t" + key + " = [" + ", ".join(map(lambda x: str(x), val)) + "]")
            return "\n".join(result)
        def dump_map_with_history(m):
            result = []
            for key, val in m.items():
                result.append("\t\t\t" + key + " = [" + str(val) + "]")
            return "\n".join(result)
        pattern = "<BookmakerHistory 1={}\n\t\tx={}\n\t\t2={}\n\t\t1x={}\n\t\t12={}\n\t\tx2={}\n\t\t" \
            "w1={}\n\t\tw2={}\n\t\tscore={}\n\t\tnotscore={}\n\t\todd={}\n\t\teven={}\n\t\t" \
            "correctscores={}\n\t\tovertotals={}\n\t\tundertotals={}\n\tasianhandicaps={}\n\teuropeanhandicaps={} />"
        return pattern.format(self._home_win, self._draw, self._away_win,
            self._not_away_win, self._not_draw, self._not_home_win,
            self._draw_no_bet_home, self._draw_no_bet_away,
            self._both_teams_score, self._both_teams_not_score,
            self._odd_goals, self._even_goals,
            dump_map(self._correct_scores),
            dump_map(self._over_totals), dump_map(self._under_totals),
            dump_map_with_history(self._asian_handicaps), dump_map_with_history(self._european_handicaps)
        )

class MatchSnapshot:
    def __init__(self, json):
        self._match = Match(json['match'])
        self._bookmakers = {}
        for key, val in json['bookmakers'].items():
            self._bookmakers[key] = BookmakerSnapshot(val)

    def get_match(self):
        return self._match

    def get_bookmakers(self):
        return self._bookmakers

    def __repr__(self):
        result = ["<MatchSnapshot match=" + str(self._match) + " bookmakers=["]
        for key, val in self._bookmakers.items():
            result.append("\t" + key + " = " + str(val))
        result.append("] />")
        return "\n".join(result)

class MatchHistory:
    def __init__(self, json):
        self._match = Match(json['match'])
        self._bookmakers = {}
        for key, val in json['bookmakers'].items():
            self._bookmakers[key] = BookmakerHistory(val)

    def get_match(self):
        return self._match

    def get_bookmakers(self):
        return self._bookmakers

    def __repr__(self):
        result = ["<MatchHistory match=" + str(self._match) + " bookmakers=["]
        for key, val in self._bookmakers.items():
            result.append("\t" + key + " = " + str(val))
        result.append("] />")
        return "\n".join(result)
